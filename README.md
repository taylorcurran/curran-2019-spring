# Project 1: Idaho Hot Springs

Currently this app pulls in data for 5 different hot springs from a JSON file.

## Requirements
### 2 Activities

Activities: 
- Main Activity - home screen 
- Maps Activity - google maps activity
- List Activity - displays a list of hotsprings 
- Login Activity - allows users to log in and register 

### 2 Custom Fragments on the same Activity

Fragments in List Activity:
- HotSpringsListFragment - displays a list of hot springs
- HotSpringDetailsFragment - displays more details about the hot springs when clicked on in the list fragment

### Reuse 1 custom fragment 

- HotSpringsListFragment is searchable and reused when something is searched 
- also reused on when the user logs in. Currently it is just a list of all the hot springs, eventually this will display only saved hot springs. 

### Database to store data between launches 

- a Database is used to save a users log in information. 
- eventually when a user saves a springs it will save it in another table. 

## Current Issues 
- the maps view does not allow you to search or zoom (no idea why)
- the list view SAVE button currently does nothing
- the search bar at the top of the list is not left aligned 
- when logging in or registering the snackbar is hidden behind the bottom nav bar 
