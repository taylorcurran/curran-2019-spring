package com.example.homework1

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import java.lang.NumberFormatException

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val editText1 = findViewById<EditText>(R.id.editText)
        val editText2 = findViewById<EditText>(R.id.editText2)

        val concatButton = findViewById<Button>(R.id.concat_button)
        val addButton = findViewById<Button>(R.id.add_button)

        val textView1 = findViewById<TextView>(R.id.textView)
        val textView2 = findViewById<TextView>(R.id.textView2)

        concatButton.setOnClickListener {
            textView1.text = "${editText1.text} ${editText2.text}"
        }

        addButton.setOnClickListener {
            try {
                val add = "${editText1.text}".toInt() + "${editText2.text}".toInt()
                textView2.text = add.toString()

            } catch (nfe: NumberFormatException) {
                textView2.text = "0"
            }
        }
    }
}
