package com.example.idahohotsprings.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import com.example.idahohotsprings.HotSpring
import com.example.idahohotsprings.R
import com.example.idahohotsprings.fragments.HotSpringListFragment.OnListFragmentInteractionListener
import kotlinx.android.synthetic.main.fragment_hotspring.view.*
import kotlinx.android.synthetic.main.fragment_item_city.view.*
import kotlinx.android.synthetic.main.fragment_item_temp_f.view.*


/**
 * [RecyclerView.Adapter] that can display a [HotSpring] and makes a call to the
 * specified [OnListFragmentInteractionListener].
 */
class ListRecyclerViewAdapter(private val mValues: List<HotSpring>,
                              private val mListener: OnListFragmentInteractionListener?)
    : RecyclerView.Adapter<ListRecyclerViewAdapter.ViewHolder>() {

    private val mOnClickListener: View.OnClickListener

    init {
        mOnClickListener = View.OnClickListener { v ->
            val item = v.tag as HotSpring

            // Notify the active callbacks interface (the activity, if the fragment is attached to
            // one) that an item has been selected.
            mListener?.onListFragmentInteraction(item)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.fragment_hotspring, parent, false)

        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = mValues[position]
        holder.mNameView.text = item.name
        holder.mCityView.text = item.city
        holder.mTempFView.text = item.temp_f.toString()

        with(holder.mView) {
            tag = item
            setOnClickListener(mOnClickListener)
        }

    }

    override fun getItemCount(): Int = mValues.size

    inner class ViewHolder(val mView: View) : RecyclerView.ViewHolder(mView) {
        val mNameView: TextView = mView.item_name
        val mCityView: TextView = mView.item_city
        val mTempFView: TextView = mView.item_temp_f
        val saveButton : Button = mView.findViewById(R.id.button_save)

        override fun toString(): String {
            return super.toString() + " '" + mCityView.text + "'"
        }
    }
}
