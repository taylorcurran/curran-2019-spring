package com.example.idahohotsprings

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.example.idahohotsprings.fragments.HotSpringListFragment
import kotlinx.android.synthetic.main.activity_users_list.*

class UsersListActivity : AppCompatActivity(), HotSpringListFragment.OnListFragmentInteractionListener {

    lateinit var listFragment: HotSpringListFragment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_users_list)

        val emailFromIntent = intent.getStringExtra("EMAIL")
        textViewName.text = emailFromIntent

        listFragment = HotSpringListFragment.newInstance(1, true)
    }

   override fun onListFragmentInteraction(item: HotSpring) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}
