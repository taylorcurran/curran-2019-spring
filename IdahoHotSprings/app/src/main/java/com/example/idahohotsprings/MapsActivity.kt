package com.example.idahohotsprings

import android.app.Activity
import android.content.Intent
import android.content.IntentSender
import android.content.pm.PackageManager
import android.graphics.BitmapFactory
import android.location.Location
import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v4.app.ActivityCompat
import android.support.v7.app.AppCompatActivity
import com.example.idahohotsprings.MainActivity.Companion.markers
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import kotlinx.android.synthetic.main.bottom_navigation.*


class MapsActivity : AppCompatActivity(), OnMapReadyCallback, GoogleMap.OnMarkerClickListener {

    private lateinit var mMap: GoogleMap
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private lateinit var lastLocation: Location
    private lateinit var locationCallback: LocationCallback
    private lateinit var locationRequest: LocationRequest
    private var locationUpdateState = false

    companion object {
        private const val LOCATION_PERMISSION_REQUEST_CODE = 1

        //REQUEST_CHECK_SETTINGS is used as the request code passed to onActivityResult.
        private const val REQUEST_CHECK_SETTINGS = 2
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maps)

        bottom_navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
        bottom_navigation.menu.findItem(R.id.navigation_home).isChecked = false
        bottom_navigation.menu.findItem(R.id.navigation_map).isChecked = true

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)

        locationCallback = object : LocationCallback() {
            override fun onLocationResult(p0: LocationResult) {
                super.onLocationResult(p0)

                lastLocation = p0.lastLocation
            }
        }

        createLocationRequest()
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap

        mMap.uiSettings.isZoomControlsEnabled = true
        mMap.setOnMarkerClickListener(this)

        setUpMap()

        //Add markers from json file
        if(markers != null) {
            for(hotSpring in markers!!.hotSprings) {

                // and move the map's camera to the same location.
                val sydney = LatLng(hotSpring.lat, hotSpring.lng)
                mMap.addMarker(
                    MarkerOptions().position(sydney)
                        .title(hotSpring.name))
                mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney))
                //placeMarkerOnMap(LatLng(hotSpring.latitude, hotSpring.longitude), user = false)
            }
        }

    }

    override fun onMarkerClick(p0: Marker?) = false

    private fun setUpMap() {
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION)
            != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this, arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION),
                LOCATION_PERMISSION_REQUEST_CODE)

            return
        }

        fusedLocationClient.lastLocation
            .addOnSuccessListener(this) { location : Location? ->
            // Got last known location. In some rare situations this can be null.
            if (location != null) {
                lastLocation = location
                val currentLatLng = LatLng(location.latitude, location.longitude)
                placeMarkerOnMap(currentLatLng, user = true)
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLatLng, 8f))

                //mMap.isMyLocationEnabled = true
                //mMap.uiSettings.isMyLocationButtonEnabled = true
            }
        }
    }


    /**
     * Place markers on map
     */
    private fun placeMarkerOnMap(location: LatLng, user: Boolean ) {
        // Creates a marker object
        val markerOptions = MarkerOptions().position(location)

        if (user) {
            markerOptions.icon(
                BitmapDescriptorFactory.fromBitmap(
                    BitmapFactory.decodeResource(resources, R.mipmap.ic_user_location)))
        } else {
            markerOptions.icon(
                BitmapDescriptorFactory.defaultMarker())
        }

        // adds it to the map
        mMap.addMarker(markerOptions)
    }

    /**
     * Receiving Location Updates
     */
    private fun startLocationUpdates() {
        //In startLocationUpdates(), if the ACCESS_FINE_LOCATION permission has not been granted,
        // request it now and return.
        if (ActivityCompat.checkSelfPermission(
                this,
                android.Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION),
                LOCATION_PERMISSION_REQUEST_CODE
            )
            return
        }

        //If there is permission, request for location updates.
        fusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, null /* Looper */)
    }

    private fun createLocationRequest() {
        // create an instance of LocationRequest, add it to an instance of LocationSettingsRequest.
        // Builder and retrieve and handle any changes to be made based on the current state of
        // the user’s location settings
        locationRequest = LocationRequest()

        // interval specifies the rate at which your app will like to receive updates
        locationRequest.interval = 10000

        // fastestInterval specifies the fastest rate at which the app can handle updates.
        locationRequest.fastestInterval = 5000
        locationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY

        val builder = LocationSettingsRequest.Builder()
            .addLocationRequest(locationRequest)

        // create a settings client and a task to check location settings.
        val client = LocationServices.getSettingsClient(this)
        val task = client.checkLocationSettings(builder.build())

        // A task success means all is well and you can go ahead and initiate a location request.
        task.addOnSuccessListener {
            locationUpdateState = true
            startLocationUpdates()
        }
        task.addOnFailureListener { e ->
            // A task failure means the location settings have some issues which can be fixed.
            // This could be as a result of the user’s location settings turned off.
            if (e is ResolvableApiException) {
                // Location settings are not satisfied, but this can be fixed
                // by showing the user a dialog.
                try {
                    // Show the dialog by calling startResolutionForResult(),
                    // and check the result in onActivityResult().
                    e.startResolutionForResult(
                        this@MapsActivity,
                        REQUEST_CHECK_SETTINGS
                    )
                } catch (sendEx: IntentSender.SendIntentException) {
                    // Ignore the error.
                }
            }
        }
    }

    /**
     * Override AppCompatActivity’s onActivityResult() method and start the update request
     * if it has a RESULT_OK result for a REQUEST_CHECK_SETTINGS request.
     */
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_CHECK_SETTINGS) {
            if (resultCode == Activity.RESULT_OK) {
                locationUpdateState = true
                startLocationUpdates()
            }
        }
    }

    /**
     * Override onPause() to stop location update request
     */
    public override fun onPause() {
        super.onPause()
    }

    /**
     * Override onResume() to restart the location update request.
     */
    public override fun onResume() {
        super.onResume()
        if (!locationUpdateState) {
            startLocationUpdates()
        }
    }

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.navigation_home -> {
                //start the Main activity
                item.isChecked = !item.isChecked
                val intent: Intent = Intent(this, MainActivity::class.java)
                startActivity(intent)
            }
            R.id.navigation_map -> {
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_list -> {
                //start the List activity
                item.isChecked = !item.isChecked
                val intent: Intent = Intent(this, ListActivity::class.java)
                startActivity(intent)
            }
            R.id.navigation_user -> {
                //start the Login activity
                item.isChecked = !item.isChecked
                val intent: Intent = Intent(this, LoginActivity::class.java)
                startActivity(intent)
            }
        }
        false
    }
}