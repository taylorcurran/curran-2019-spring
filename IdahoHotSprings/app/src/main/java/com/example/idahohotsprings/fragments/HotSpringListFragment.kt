package com.example.idahohotsprings.fragments

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.idahohotsprings.HotSpring
import com.example.idahohotsprings.LoginActivity.Companion.databaseHelper
import com.example.idahohotsprings.LoginActivity.Companion.email
import com.example.idahohotsprings.MainActivity.Companion.markers
import com.example.idahohotsprings.R
import com.example.idahohotsprings.adapters.ListRecyclerViewAdapter


/**
 * A fragment representing a list of Items.
 * Activities containing this fragment MUST implement the
 * [HotSpringListFragment.OnListFragmentInteractionListener] interface.
 */
class HotSpringListFragment : Fragment() {

    private var columnCount = 1
    private var savedView = false
    private var searchQuery = ""

    private var listener: OnListFragmentInteractionListener? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            columnCount = it.getInt(ARG_COLUMN_COUNT)
            savedView = it.getBoolean(ARG_SAVED_VIEW)
            searchQuery = it.getString(ARG_SEARCH_QUERY)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_hotspring_list, container, false)

        // Set the adapter
        if (view is RecyclerView) {
            with(view) {
                layoutManager = when {
                    columnCount <= 1 -> LinearLayoutManager(context)
                    else -> GridLayoutManager(context, columnCount)
                }
                if (markers != null) {

                    if (savedView) {
                        val savedHotSpring : MutableList<HotSpring>? = ArrayList()
                        val databaseSprings = databaseHelper.getAllUsersSavedSprings(email)

                        markers!!.hotSprings.forEach {
                            if (databaseSprings.contains(it.name)) {
                                savedHotSpring?.add(it)
                            }
                        }
                        if (!savedHotSpring.isNullOrEmpty()) {
                            adapter = ListRecyclerViewAdapter(
                                savedHotSpring,
                                listener
                            )

                            return view
                        }
                    }

                    if(searchQuery.isNotEmpty()) {
                        val filteredMarkers : MutableList<HotSpring>? = ArrayList()
                        for (spring in markers!!.hotSprings) {
                            // name match condition. this might differ depending on your requirement
                            // here we are looking for name or phone number match
                            if (spring.name.toLowerCase().contains(searchQuery.toLowerCase()) ||
                                spring.city.toLowerCase().contains(searchQuery.toLowerCase())) {
                                filteredMarkers?.add(spring)
                            }
                        }

                        if (!filteredMarkers.isNullOrEmpty()) {
                            adapter = ListRecyclerViewAdapter(
                                filteredMarkers,
                                listener
                            )

                            return view
                        }
                    }

                    adapter = ListRecyclerViewAdapter(
                        markers!!.hotSprings,
                        listener
                    )
                }
            }
        }
        return view
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnListFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException("$context must implement OnListFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson
     * [Communicating with Other Fragments](http://developer.android.com/training/basics/fragments/communicating.html)
     * for more information.
     */
    interface OnListFragmentInteractionListener {
        fun onListFragmentInteraction(item: HotSpring)
    }

    companion object {

        const val ARG_COLUMN_COUNT = "column-count"
        const val ARG_SAVED_VIEW = "saved-view"
        const val ARG_SEARCH_QUERY = "search-query"

        @JvmStatic
        fun newInstance(columnCount: Int, savedView: Boolean) =
            HotSpringListFragment().apply {
                arguments = Bundle().apply {
                    putInt(ARG_COLUMN_COUNT, columnCount)
                    putBoolean(ARG_SAVED_VIEW, savedView)
                    putString(ARG_SEARCH_QUERY, searchQuery)
                }
            }
    }

}