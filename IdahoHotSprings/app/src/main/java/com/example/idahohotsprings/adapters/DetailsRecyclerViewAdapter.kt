package com.example.idahohotsprings.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import com.example.idahohotsprings.HotSpring
import com.example.idahohotsprings.LoginActivity.Companion.loggedIn
import com.example.idahohotsprings.R
import com.example.idahohotsprings.fragments.HotSpringListFragment.OnListFragmentInteractionListener
import kotlinx.android.synthetic.main.fragment_details.view.*
import kotlinx.android.synthetic.main.fragment_hotspring.view.item_name
import kotlinx.android.synthetic.main.fragment_item_city.view.*
import kotlinx.android.synthetic.main.fragment_item_temp_f.view.*


/**
 * [RecyclerView.Adapter] that can display a [HotSpring] and makes a call to the
 * specified [OnListFragmentInteractionListener].
 */
class DetailsRecyclerViewAdapter(private val mValues: HotSpring)
    : RecyclerView.Adapter<DetailsRecyclerViewAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.fragment_details, parent, false)

        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = mValues
        holder.mNameView.text = item.name
        holder.mCityView.text = item.city
        holder.mTempFView.text = item.temp_f.toString()
        if(item.description != null) {
            holder.mDescriptionView.text = item.description
        } else {
            holder.mDescriptionView.visibility = View.GONE
        }
        if(loggedIn) {
          //check to see if Hot Springs is already saved

        }


    }

    override fun getItemCount(): Int { return 1 }

    inner class ViewHolder(val mView: View) : RecyclerView.ViewHolder(mView) {
        val mNameView: TextView = mView.item_name
        val mCityView: TextView = mView.item_city
        val mTempFView: TextView = mView.item_temp_f
        val mDescriptionView : TextView = mView.item_description
        val saveButton : Button = mView.findViewById(R.id.button_save)

        override fun toString(): String {
            return super.toString() + " '" + mNameView.text + "'"
        }
    }
}
