package com.example.idahohotsprings

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.*
import android.support.v4.widget.NestedScrollView
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.AppCompatButton
import android.support.v7.widget.AppCompatTextView
import android.view.Gravity
import android.view.View
import com.example.idahohotsprings.Login.DatabaseHelper
import com.example.idahohotsprings.Login.InputValidation
import com.example.idahohotsprings.Login.User
import kotlinx.android.synthetic.main.bottom_navigation.*

class RegisterActivity:AppCompatActivity(), View.OnClickListener {
    private val activity = this@RegisterActivity
    private lateinit var nestedScrollView:NestedScrollView
    private lateinit var textInputLayoutName:TextInputLayout
    private lateinit var textInputLayoutEmail:TextInputLayout
    private lateinit var textInputLayoutPassword:TextInputLayout
    private lateinit var textInputLayoutConfirmPassword:TextInputLayout
    private lateinit var textInputEditTextName:TextInputEditText
    private lateinit var textInputEditTextEmail:TextInputEditText
    private lateinit var textInputEditTextPassword:TextInputEditText
    private lateinit var textInputEditTextConfirmPassword:TextInputEditText
    private lateinit var appCompatButtonRegister:AppCompatButton
    private lateinit var appCompatTextViewLoginLink:AppCompatTextView
    private lateinit var inputValidation:InputValidation
    private lateinit var databaseHelper:DatabaseHelper
    private lateinit var user:User


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        bottom_navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
        bottom_navigation.menu.findItem(R.id.navigation_home).isChecked = false
        bottom_navigation.menu.findItem(R.id.navigation_map).isChecked = false
        bottom_navigation.menu.findItem(R.id.navigation_list).isChecked = false
        bottom_navigation.menu.findItem(R.id.navigation_user).isChecked = true

        //supportActionBar!!.hide()
        initViews()
        initListeners()
        initObjects()
    }
    /**
     * This method is to initialize views
     */
    private fun initViews() {
        nestedScrollView = findViewById<NestedScrollView>(R.id.nestedScrollView)
        textInputLayoutName = findViewById<TextInputLayout>(R.id.textInputLayoutName)
        textInputLayoutEmail = findViewById<TextInputLayout>(R.id.textInputLayoutEmail)
        textInputLayoutPassword = findViewById<TextInputLayout>(R.id.textInputLayoutPassword)
        textInputLayoutConfirmPassword = findViewById<TextInputLayout>(R.id.textInputLayoutConfirmPassword)
        textInputEditTextName = findViewById<TextInputEditText>(R.id.textInputEditTextName)
        textInputEditTextEmail = findViewById<TextInputEditText>(R.id.textInputEditTextEmail)
        textInputEditTextPassword = findViewById<TextInputEditText>(R.id.textInputEditTextPassword)
        textInputEditTextConfirmPassword = findViewById<TextInputEditText>(R.id.textInputEditTextConfirmPassword)
        appCompatButtonRegister = findViewById<AppCompatButton>(R.id.appCompatButtonRegister)
        appCompatTextViewLoginLink = findViewById<AppCompatTextView>(R.id.appCompatTextViewLoginLink)
    }
    /**
     * This method is to initialize listeners
     */
    private fun initListeners() {
        appCompatButtonRegister.setOnClickListener(this)
        appCompatTextViewLoginLink.setOnClickListener(this)
    }
    /**
     * This method is to initialize objects to be used
     */
    private fun initObjects() {
        inputValidation = InputValidation(activity)
        databaseHelper = DatabaseHelper(activity)
        user = User()
    }
    /**
     * This implemented method is to listen the click on view
     *
     * @param v
     */
    override fun onClick(v:View) {
        when (v.id) {
            R.id.appCompatButtonRegister -> postDataToSQLite()
            R.id.appCompatTextViewLoginLink -> finish()
        }
    }
    /**
     * This method is to validate the input text fields and post data to SQLite
     */
    private fun postDataToSQLite() {
        if (!inputValidation.isInputEditTextFilled(textInputEditTextName, textInputLayoutName, getString(R.string.error_message_name))) {
            return
        }
        if (!inputValidation.isInputEditTextFilled(textInputEditTextEmail, textInputLayoutEmail, getString(R.string.error_message_email)))
        {
            return
        }
        if (!inputValidation.isInputEditTextEmail(textInputEditTextEmail, textInputLayoutEmail, getString(R.string.error_message_email)))
        {
            return
        }
        if (!inputValidation.isInputEditTextFilled(textInputEditTextPassword, textInputLayoutPassword, getString(R.string.error_message_password)))
        {
            return
        }
        if (!inputValidation.isInputEditTextMatches(textInputEditTextPassword, textInputEditTextConfirmPassword,
                textInputLayoutConfirmPassword, getString(R.string.error_password_match)))
        {
            return
        }
        if (!databaseHelper.checkUser(textInputEditTextEmail.text.toString().trim()))
        {
            user.name = (textInputEditTextName.text.toString().trim())
            user.email = (textInputEditTextEmail.text.toString().trim())
            user.password = (textInputEditTextPassword.text.toString().trim())
            databaseHelper.addUser(user)

            // Snack Bar to show success message that record saved successfully
            var snackbar =  Snackbar.make(findViewById(R.id.coordinator), getString(R.string.success_message), Snackbar.LENGTH_LONG)

            val layoutParams = snackbar.view.layoutParams as CoordinatorLayout.LayoutParams
            layoutParams.anchorId = R.id.bottom_navigation //Id for your bottomNavBar or TabLayout
            layoutParams.anchorGravity = Gravity.TOP
            layoutParams.gravity = Gravity.TOP
            snackbar.view.layoutParams = layoutParams

            snackbar.show()

            emptyInputEditText()


            // Navigate to LoginActivity
            //val intentLogin = Intent(applicationContext, LoginActivity::class.java)
            //startActivity(intentLogin)
        }
        else
        {
            // Snack Bar to show error message that record already exists
            var snackbar =  Snackbar.make(findViewById(R.id.coordinator), getString(R.string.error_email_exists), Snackbar.LENGTH_LONG)

            val layoutParams = snackbar.view.layoutParams as CoordinatorLayout.LayoutParams
            layoutParams.anchorId = R.id.bottom_navigation //Id for your bottomNavBar or TabLayout
            layoutParams.anchorGravity = Gravity.TOP
            layoutParams.gravity = Gravity.TOP
            snackbar.view.layoutParams = layoutParams

            snackbar.show()
        }
    }
    /**
     * This method is to empty all input edit text
     */
    private fun emptyInputEditText() {
        textInputEditTextName.text = null
        textInputEditTextEmail.text = null
        textInputEditTextPassword.text = null
        textInputEditTextConfirmPassword.text = null
    }

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.navigation_home -> {
                //start the Main activity
                val intent: Intent = Intent(this, MainActivity::class.java)
                startActivity(intent)
            }
            R.id.navigation_map -> {
                //start the Maps activity
                val intent: Intent = Intent(this, MapsActivity::class.java)
                startActivity(intent)
            }
            R.id.navigation_list -> {
                //start the List activity
                val intent: Intent = Intent(this, ListActivity::class.java)
                startActivity(intent)
            }
            R.id.navigation_user -> {
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

}
