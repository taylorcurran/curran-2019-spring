package com.example.idahohotsprings

import java.io.Serializable

data class HotSpring (
    val name: String,
    val lat: Double,
    val lng: Double,
    val temp_f: Int? = null,
    val temp_c: Int? = null,
    val city: String,
    val commercial: Boolean? = false,
    val description: String? = null
) : Serializable
