package com.example.idahohotsprings

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.*
import android.support.v4.widget.NestedScrollView
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.AppCompatButton
import android.support.v7.widget.AppCompatTextView
import android.view.Gravity
import android.view.View
import com.example.idahohotsprings.Login.DatabaseHelper
import com.example.idahohotsprings.Login.InputValidation
import kotlinx.android.synthetic.main.activity_maps.*




class LoginActivity:AppCompatActivity(), View.OnClickListener {
    private val activity = this@LoginActivity
    private lateinit var nestedScrollView:NestedScrollView
    private lateinit var textInputLayoutEmail:TextInputLayout
    private lateinit var textInputLayoutPassword:TextInputLayout
    private lateinit var textInputEditTextEmail:TextInputEditText
    private lateinit var textInputEditTextPassword:TextInputEditText
    private lateinit var appCompatButtonLogin:AppCompatButton
    private lateinit var textViewLinkRegister:AppCompatTextView
    private lateinit var inputValidation:InputValidation


    companion object {
        lateinit var databaseHelper:DatabaseHelper
        var loggedIn : Boolean = false
        var email : String = ""
        lateinit var savedSpring : HotSpring
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_login)

        bottom_navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
        bottom_navigation.menu.findItem(R.id.navigation_home).isChecked = false
        bottom_navigation.menu.findItem(R.id.navigation_map).isChecked = false
        bottom_navigation.menu.findItem(R.id.navigation_list).isChecked = false
        bottom_navigation.menu.findItem(R.id.navigation_user).isChecked = true


        initViews()
        initListeners()
        initObjects()
    }

    /**
     * This method is to initialize views
     */
    private fun initViews() {
        nestedScrollView = findViewById<NestedScrollView>(R.id.nestedScrollView)
        textInputLayoutEmail = findViewById<TextInputLayout>(R.id.textInputLayoutEmail)
        textInputLayoutPassword = findViewById<TextInputLayout>(R.id.textInputLayoutPassword)
        textInputEditTextEmail = findViewById<TextInputEditText>(R.id.textInputEditTextEmail)
        textInputEditTextPassword = findViewById<TextInputEditText>(R.id.textInputEditTextPassword)
        appCompatButtonLogin = findViewById<AppCompatButton>(R.id.appCompatButtonLogin)
        textViewLinkRegister = findViewById<AppCompatTextView>(R.id.textViewLinkRegister)
    }
    /**
     * This method is to initialize listeners
     */
    private fun initListeners() {
        appCompatButtonLogin.setOnClickListener(this)
        textViewLinkRegister.setOnClickListener(this)
    }
    /**
     * This method is to initialize objects to be used
     */
    private fun initObjects() {
        databaseHelper = DatabaseHelper(activity)
        inputValidation = InputValidation(activity)
    }
    /**
     * This implemented method is to listen the click on view
     *
     * @param v
     */
    override fun onClick(v:View) {
        when (v.id) {
            R.id.appCompatButtonLogin -> verifyFromSQLite()
            R.id.textViewLinkRegister -> {
                // Navigate to RegisterActivity
                val intentRegister = Intent(applicationContext, RegisterActivity::class.java)
                startActivity(intentRegister)
            }
        }
    }
    /**
     * This method is to validate the input text fields and verify login credentials from SQLite
     */
    private fun verifyFromSQLite() {
        if (!inputValidation.isInputEditTextFilled(textInputEditTextEmail, textInputLayoutEmail, getString(R.string.error_message_name)))
        {
            return
        }
        if (!inputValidation.isInputEditTextEmail(textInputEditTextEmail, textInputLayoutEmail, getString(R.string.error_message_email)))
        {
            return
        }
        if (!inputValidation.isInputEditTextFilled(textInputEditTextPassword, textInputLayoutPassword, getString(R.string.error_message_password)))
        {
            return
        }
        if (databaseHelper.checkUser(textInputEditTextEmail.text.toString().trim(), textInputEditTextPassword.text.toString().trim()))
        {
            loggedIn = true
            email = textInputEditTextEmail.text.toString().trim()
            val accountsIntent = Intent(activity, UsersListActivity::class.java)
            accountsIntent.putExtra("EMAIL", textInputEditTextEmail.text.toString().trim())
            emptyInputEditText()
            startActivity(accountsIntent)

            //persist credentials shared preferences
        }
        else
        {
            // Snack Bar to show success message that record is wrong
            var snackbar =  Snackbar.make(findViewById(R.id.coordinator), getString(R.string.error_valid_email_password), Snackbar.LENGTH_LONG)

            val layoutParams = snackbar.view.layoutParams as CoordinatorLayout.LayoutParams
            layoutParams.anchorId = R.id.bottom_navigation //Id for your bottomNavBar or TabLayout
            layoutParams.anchorGravity = Gravity.TOP
            layoutParams.gravity = Gravity.TOP
            snackbar.view.layoutParams = layoutParams

            snackbar.show()

        }
    }
    /**
     * This method is to empty all input edit text
     */
    private fun emptyInputEditText() {
        textInputEditTextEmail.text = null
        textInputEditTextPassword.text = null
    }

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.navigation_home -> {
                //start the Main activity
                val intent: Intent = Intent(this, MainActivity::class.java)
                startActivity(intent)
            }
            R.id.navigation_map -> {
                //start the Maps activity
                val intent: Intent = Intent(this, MapsActivity::class.java)
                startActivity(intent)
            }
            R.id.navigation_list -> {
                //start the List activity
                val intent: Intent = Intent(this, ListActivity::class.java)
                startActivity(intent)
            }
            R.id.navigation_user -> {
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }
}
