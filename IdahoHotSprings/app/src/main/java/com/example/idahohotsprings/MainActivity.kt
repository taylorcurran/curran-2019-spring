package com.example.idahohotsprings

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import com.squareup.moshi.Moshi
import kotlinx.android.synthetic.main.bottom_navigation.*

class MainActivity : AppCompatActivity() {

    companion object {
        var markers : Markers? = null
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //Sets the toolbar as the appbar for the activity
        setSupportActionBar(findViewById(R.id.toolbar))

        bottom_navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
        bottom_navigation.menu.findItem(R.id.navigation_home).isChecked = true

        //populate the Markers object
        val fileName = "idahohotsprings.json"
        val jsonString = application.assets.open(fileName).bufferedReader().use{
            it.readText()
        }
        parseJSON(jsonString)
    }

    private fun parseJSON(json: String) {
        val moshi = Moshi.Builder()
            .add(DefaultOnDataMismatchAdapter.newFactory(HotSpring::class.java, null))
            .build()
        val jsonAdapter = moshi.adapter(Markers::class.java)

        try {
            markers = jsonAdapter.fromJson(json)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.toolbar, menu)

        return true
    }

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.navigation_home -> {
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_map -> {
                //start the Maps activity
                val intent: Intent = Intent(this, MapsActivity::class.java)
                startActivity(intent)
            }
            R.id.navigation_list -> {
                //start the List activity
                val intent: Intent = Intent(this, ListActivity::class.java)
                startActivity(intent)
            }
            R.id.navigation_user -> {
                //start the Login activity
                val intent: Intent = Intent(this, LoginActivity::class.java)
                startActivity(intent)
            }
        }
        false
    }
}
