package com.example.idahohotsprings.fragments

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.idahohotsprings.HotSpring
import com.example.idahohotsprings.MainActivity
import com.example.idahohotsprings.R
import com.example.idahohotsprings.adapters.DetailsRecyclerViewAdapter


/**
 * A simple [Fragment] subclass.
 * Use the [DetailsFragment.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class DetailsFragment : Fragment() {
    private var hotSpring: HotSpring? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            hotSpring = it.get(HOT_SPRING) as HotSpring?
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_details_list, container, false)

        // Set the adapter
        if (view is RecyclerView) {
            with(view) {
                layoutManager = LinearLayoutManager(context)

                if (MainActivity.markers != null) {

                    adapter = hotSpring?.let {
                        DetailsRecyclerViewAdapter(
                            it
                        )
                    }
                }
            }
        }
        return view
    }

    companion object {

        const val HOT_SPRING = "hot-spring"

        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @return A new instance of fragment DetailsFragment.
         */
        @JvmStatic
        fun newInstance(hotSpring: HotSpring) =
            DetailsFragment().apply {
                arguments = Bundle().apply {
                    putSerializable(HOT_SPRING, hotSpring)
                }
            }
    }
}

