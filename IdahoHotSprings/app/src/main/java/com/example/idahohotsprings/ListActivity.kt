package com.example.idahohotsprings

import android.app.ActionBar
import android.app.SearchManager
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.widget.FrameLayout
import android.widget.SearchView
import com.example.idahohotsprings.fragments.DetailsFragment
import com.example.idahohotsprings.fragments.HotSpringListFragment
import kotlinx.android.synthetic.main.bottom_navigation.*


class ListActivity : AppCompatActivity(), HotSpringListFragment.OnListFragmentInteractionListener {

    lateinit var listFragment: HotSpringListFragment
    lateinit var detailsFragment: DetailsFragment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_scrolling)

        //Sets the toolbar as the appbar for the activity
        setSupportActionBar(findViewById(R.id.toolbar))

        bottom_navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
        bottom_navigation.menu.findItem(R.id.navigation_home).isChecked = false
        bottom_navigation.menu.findItem(R.id.navigation_map).isChecked = false
        bottom_navigation.menu.findItem(R.id.navigation_list).isChecked = true


        // Check that the activity is using the layout version with
        // the fragment_container FrameLayout
        if (findViewById<FrameLayout>(R.id.fragment_container) != null) {

            // However, if we're being restored from a previous state,
            // then we don't need to do anything and should return or else
            // we could end up with overlapping fragments.
            if (savedInstanceState != null) {
                return
            }

            // Create a new Fragment to be placed in the activity layout
            val firstFragment = HotSpringListFragment()

            // In case this activity was started with special instructions from an
            // Intent, pass the Intent's extras to the fragment as arguments
            firstFragment.arguments = intent.extras

            // Add the fragment to the 'fragment_container' FrameLayout
            supportFragmentManager.beginTransaction()
                .add(R.id.fragment_container, firstFragment).commit()

            //Search
            handleIntent(intent)
        }
    }

    override fun onNewIntent(intent: Intent) {
        handleIntent(intent)
    }

    private fun handleIntent(intent: Intent) {

        if (Intent.ACTION_SEARCH == intent.action) {
            var query = intent.getStringExtra(SearchManager.QUERY)
            //use the query to search your data somehow

            if (query.isNotEmpty()) {
                // Create fragment and give it an argument specifying the article it should show
                val newFragment = HotSpringListFragment()
                val args = Bundle()
                args.putString(HotSpringListFragment.ARG_SEARCH_QUERY, query)
                newFragment.arguments = args

                val transaction = supportFragmentManager.beginTransaction()

                // Replace whatever is in the fragment_container view with this fragment,
                // and add the transaction to the back stack so the user can navigate back
                transaction.replace(R.id.fragment_container, newFragment)
                transaction.addToBackStack(null)

                // Commit the transaction
                transaction.commit()
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.search_menu, menu)

        val searchViewItem = menu.findItem(R.id.search)
        val searchView = searchViewItem.actionView as SearchView
        searchView.setIconifiedByDefault(false)
        val params = ActionBar.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.MATCH_PARENT)
        searchView.layoutParams = params
        supportActionBar?.title = null
        searchViewItem.expandActionView()

        // Associate searchable configuration with the SearchView
        val searchManager = getSystemService(Context.SEARCH_SERVICE) as SearchManager
        (menu.findItem(R.id.search).actionView as SearchView).apply {
            setSearchableInfo(searchManager.getSearchableInfo(componentName))
        }

        return true
    }

    override fun onListFragmentInteraction(item: HotSpring) {

        // Create fragment and give it an argument specifying the article it should show
        val newFragment = DetailsFragment()
        val args = Bundle()
        args.putSerializable(DetailsFragment.HOT_SPRING, item)
        newFragment.arguments = args

        val transaction = supportFragmentManager.beginTransaction()

        // Replace whatever is in the fragment_container view with this fragment,
        // and add the transaction to the back stack so the user can navigate back
        transaction.replace(R.id.fragment_container, newFragment)
        transaction.addToBackStack(null)

        // Commit the transaction
        transaction.commit()

/*        // Begin transaction.
        val beginTransaction = fragmentManager.beginTransaction()
        // Replace the layout holder with the required Fragment object.
        beginTransaction.replace(newFragment.id, newFragment)
        beginTransaction.addToBackStack(null)
        // Commit transaction.
        beginTransaction.commit()*/

        /*if(loggedIn) {
            //save spring data
            val savedList = databaseHelper.getAllUsersSavedSprings()

            if(!savedList.contains(item.name)) {
                val userSavedSprings : UserSavedSprings? = null
                userSavedSprings!!.email = email
                userSavedSprings.hot_spring_name = item.name

                databaseHelper.addUserSavedSprings(userSavedSprings)
            }

            HotSpringListFragment.newInstance(1, false)
        } else {
            //redirect to login page
            val intent: Intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)
        }*/
    }

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.navigation_home -> {
                //start the Main activity
                val intent: Intent = Intent(this, MainActivity::class.java)
                startActivity(intent)
            }
            R.id.navigation_map -> {
                //start the Maps activity
                val intent: Intent = Intent(this, MapsActivity::class.java)
                startActivity(intent)
            }
            R.id.navigation_list -> {
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_user -> {
                //start the Login activity
                val intent: Intent = Intent(this, LoginActivity::class.java)
                startActivity(intent)
            }
        }
        false
    }
}
